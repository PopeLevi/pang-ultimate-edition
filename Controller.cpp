#include <cstdlib>
#include <ctime>
#include <cmath>

#include <SDL_image.h>

#include "Controller.hpp"

Controller::Controller() { this->init(new WindowDetails); }

Controller::Controller(WindowDetails *details) { this->init(details); }

Controller::~Controller() { this->Cleanup(); }

// Load resources into memory, etc
void Controller::init(WindowDetails *details)
{
    quit = false;
    windowExists = false;
    wDetails = *details;
    delete details;

    if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO) < 0)
    {
	DEBUG("Unable to initialise SDL2");
	quit = true;
	return;
    } else LOG("SDL2 Initialised");

    // Initialise SDL2_image mode, so we can load PNG images into the program. If this fails, display an error message and quit
    if (!(IMG_Init(IMG_INIT_PNG) & IMG_INIT_PNG))
    {
	DEBUG("Unable to initialise SDL2_image");
        quit = true;
        return;
    } else LOG("SDL2_image initialised");
    
    // Load the logo. If this fails, quit the program
    if (!(mmLogoSurface = IMG_Load(FILE_LOGO)))
    {
	DEBUG(((std::string)"Unable to load " + (std::string)FILE_LOGO).c_str());
	quit = true;
	return;
    } else LOG(((std::string)FILE_LOGO + (std::string)" loaded").c_str());

    // Load both of the enter key images
    if (!(mmEnterPrompt1 = IMG_Load(FILE_ENTER1)))	
    {
	DEBUG(((std::string)"Unable to load " + (std::string)FILE_ENTER1).c_str());
	quit = true;
	return;
    } else LOG(((std::string)FILE_ENTER1 + (std::string)" loaded").c_str());

    if (!(mmEnterPrompt2 = IMG_Load(FILE_ENTER2)))
    {
	DEBUG(((std::string)"Unable to load " + (std::string)FILE_ENTER2).c_str());
	quit = true;
	return;
    } else LOG(((std::string)FILE_ENTER2 + (std::string)" loaded").c_str());

    // Try to load the 'bounce' visual effect from file. If this fails, display an error message and quit to prevent memory errors later on in the program
    if (!(bounceSurface = IMG_Load(FILE_PBOUNCE)))
    {
	DEBUG(((std::string)"Unable to load " + (std::string)FILE_PBOUNCE).c_str());
        quit = true;
        return;
    } else LOG(((std::string)FILE_PBOUNCE + (std::string)" loaded").c_str());

    // Initialise SDL2_Mixer for sound effects
    if (Mix_OpenAudio(MIX_DEFAULT_FREQUENCY, MIX_DEFAULT_FORMAT, 2, 2048) < 0)
    {
	DEBUG("Unable to init SDL2_mixer");
	quit = true;
	return;
    } else LOG("SDL2_mixer initialised");

    // Loop through each element of sparkSounds and assign it to a sound effect
    for (unsigned int i = 0; i < sizeof(sparkSounds) / sizeof(Mix_Chunk*); i++)
	if (!(sparkSounds[i] = Mix_LoadWAV(sparkSoundFiles[i])))
	{
	    DEBUG(((std::string)"Unable to load " + (std::string)sparkSoundFiles[i] + ": " + (std::string)Mix_GetError()).c_str());
	    quit = true;
	    return;
	} else LOG(((std::string)sparkSoundFiles[i] + (std::string)" loaded").c_str());

    // Do the same for the paddle sounds
    for (unsigned int i = 0; i < sizeof(paddleSounds) / sizeof(Mix_Chunk*); i++)
	if (!(paddleSounds[i] = Mix_LoadWAV(paddleSoundFiles[i])))
	{
	    DEBUG(((std::string)"Unable to load " + (std::string)paddleSoundFiles[i] + ": " + (std::string)Mix_GetError()).c_str());
	    quit = true;
	    return;
	} else LOG(((std::string)paddleSoundFiles[i] + (std::string)" loaded").c_str());

    if (!(powerupSound = Mix_LoadWAV(FILE_PWRUP_SOUND)))
    {
	DEBUG(((std::string)"Unable to load " + (std::string)FILE_PWRUP_SOUND + ": " + (std::string)Mix_GetError()).c_str());
	quit = true;
	return;
    } else LOG(((std::string)FILE_PWRUP_SOUND + (std::string)" loaded").c_str());

    if (!(menuClick = Mix_LoadWAV(FILE_MENU_CLICK)))
    {
	DEBUG(((std::string)"Unable to load " + (std::string)FILE_MENU_CLICK + ": " + (std::string)Mix_GetError()).c_str());
	quit = true;
	return;
    }
}

// Initialise variables and start the main game loop
void Controller::Init()
{
    if (quit) return;

    gameStarted = false;
    mouseCaptured = true;
    helpVisible = false;
    gameTicks = 0;
    lastHitTick = 0;
    flashTicks = 0;
    bounceAlpha = 255;
    lastSDLticks = 0;

    srand(time(NULL));
    ball.xvel(0);
    ball.yvel(0);

    // Randomize x velocity until != 0
    while (!ball.xvel())
        ball.xvel((rand() % 4) - (rand() % 4));

    // Randomize y velocity until != 0
    while (!ball.yvel())
        ball.yvel((rand() % 4) - (rand() % 4));

    // Center the ball in the main window
    ball.x((wDetails.width / 2) - (ball.width() / 2));
    ball.y((wDetails.height / 2) - (ball.height() / 2));

    // Set up the player by setting the screen side and window size
    ScreenSideData paddleData;
    paddleData.side = LEFT;
    paddleData.sWidth = wDetails.width;
    paddleData.sHeight = wDetails.height;
    player1Paddle.side(paddleData);

    // Reuse paddleData for the aiPaddle
    paddleData.side = RIGHT;
    aiPaddle.side(paddleData);

    // Set up the 'bounce' visual effect
    bounceRect.x = 0;
    bounceRect.y = 0;
    bounceRect.w = 48;
    bounceRect.h = 48;

    // Retrieve the screen refresh rate for usage in Controller::Idle() to create a more pleasing framerate
    SDL_DisplayMode dsm;
    SDL_GetDisplayMode(0, 0, &dsm);
    refreshRate = dsm.refresh_rate;

    // Create the main window using wDetails to style it. If this fails, display an error message and quit
    if (!windowExists)
    {
	if (!(mainWindow = SDL_CreateWindow(wDetails.title, wDetails.x, wDetails.y, wDetails.width, wDetails.height, wDetails.style)))
	{
	    DEBUG("Unable to create main window");
	    quit = true;
	    return;
	} else LOG("Main window created");

	windowExists = true;
    }

    // Get the main window surface for drawing to. If this fails, display an error message and quit
    if (!(windowSurface = SDL_GetWindowSurface(mainWindow)))
    {
	DEBUG("Unable to load window surface");
        quit = true;
        return;
    } else LOG("Main window surface loaded");

    // Load the main window icon
    if (!(iconSurface = IMG_Load(FILE_WINICON))) DEBUG(std::string(((std::string)"Unable to load " + FILE_WINICON)).c_str());
    else
    {
	SDL_SetWindowIcon(mainWindow, iconSurface); // Set the main window icon to FILE_WINICON. It doesn't matter if this fails; this is not a crucial part of the program
	LOG("Window icon loaded");
    }

    Mix_Volume(SPARK_SOUND_CHANNEL, SPARK_SOUND_VOL);
    Mix_Volume(PADDLE_SOUND_CHANNEL, PADDLE_SOUND_VOL);
    Mix_Volume(PWRUP_SOUND_CHANNEL, PWRUP_SOUND_VOL);
    Mix_Volume(MENU_SOUND_CHANNEL, MENU_SOUND_VOL);
    
    // Start the main game loop
    this->OnLoop();
}

void Controller::OnLoop()
{
    if (!quit) this->init_menu();

    gameStarted = true;
    if (!quit) LOG("Begin main game loop");
    
    while (!quit)
    {
        // Handle events until there are no events left
	SDL_Event event;
        while (SDL_PollEvent(&event))
            this->OnEvent(&event);

        // If the user requested a quit, exit the loop and end the program
        if (quit) break;

        // If the ball is touching either one of the paddles, bounce the ball
        if (player1Paddle.touching(ball.objectbox()) || aiPaddle.touching(ball.objectbox()))
        {
            // Get a pointer to the paddle touching the ball and base collision detection on this
            Paddle *temp = player1Paddle.touching(ball.objectbox()) ? &player1Paddle : &aiPaddle;

            // Set the ball x to the edge of the paddle facing inwards to avoid the ball being stuck behind the paddle forever
            ball.x(temp->side() == LEFT ? temp->x() + temp->width() : temp->x() - ball.width());

            // If the absolute value of the ball x or y velocities is larger than 4, show the 'bounce' visual effect to denote a large impact
            if (abs(ball.xvel()) > 4 || abs(ball.yvel() > 4))
            {
                // If the paddle is on the left side of the screen, set the effect to the paddle x + width - (the width of the effect / 2) otherwise
                // set the effect to the paddle x - (the width of the effect / 2). This is so the effect is only partially exposed
		bounceRect.h = ball.height() * 2;
		bounceRect.w = ball.width() * 2;
                bounceRect.x = temp->side() == LEFT ? (temp->x() + temp->width()) - (bounceRect.w / 2) : temp->x() - (bounceRect.w / 2);
                bounceRect.y = ball.y() - (ball.height() / 2);
            }

	    // Play a random impact sound
	    Mix_PlayChannel(PADDLE_SOUND_CHANNEL, paddleSounds[rand() % (sizeof(paddleSounds) / sizeof(Mix_Chunk*))], 0);

            ball.Bounce(temp->side() == LEFT ? Wall::WALL_LEFT : Wall::WALL_RIGHT);
            ball.add_hit();
            ball.paddle(temp);

	    delete temp;
	    temp = NULL;
        }

        if (ball.hits() % 5 == 0 && ball.hits()) // Randomly increase the speed of the ball every 5 hits if hits != 0 and the velocity of the ball is < 10
        {
            if (gameTicks - lastHitTick > 10) // Wait at least 10 game ticks until the speed can be changed again
            {
                bool negX = ball.xvel() < 0, negY = ball.yvel() < 0; // Check to see if the x or y velocities are negative, so the values can be inverted after processing
                int absXvel = abs(ball.xvel()), absYvel = abs(ball.yvel()); // Store the absolute values of the two velocities

                ball.xvel(absXvel + (absXvel < MAX_BALL_SPEED ? rand() % 2 : 0)); // Randomly increase the x velocity if the velocity is less than MAX_BALL_SPEED
                ball.yvel(absYvel + (absYvel < MAX_BALL_SPEED ? rand() % 2 : 0)); // Do the same for the y velocity

                if (negX) ball.xvel(-ball.xvel()); // Invert the velocities if they are supposed to be negative. If this is not done they will always move to the right side of the screen
                if (negY) ball.yvel(-ball.yvel());
            }

            lastHitTick = gameTicks;
        }

        // Prevent the ball from going more than MAX_BALL_SPEED pixels per cycle. This is to keep the game playable
        ball.x(ball.x() + (ball.xvel() > MAX_BALL_SPEED ? MAX_BALL_SPEED : ball.xvel()));
        ball.y(ball.y() + (ball.yvel() > MAX_BALL_SPEED ? MAX_BALL_SPEED : ball.yvel()));

	// Handle collision detection with walls
	if (ball.x() > wDetails.width) // Right wall (ball right >= window width)
        {
            ball.x(aiPaddle.x() - ball.width() - 10);
	    ball.y(aiPaddle.y() + (aiPaddle.height() / 2));
            ball.Bounce(Wall::WALL_RIGHT);
            flashTicks = 25;
            gameScores.player++;
        }
        else if (ball.x() < 0 - ball.width()) // Left wall (ball left <= window left)
        {
            ball.x(player1Paddle.x() + player1Paddle.width() + 10);
	    ball.y(player1Paddle.y() + (player1Paddle.height() / 2));
            ball.Bounce(Wall::WALL_LEFT);
            flashTicks = 25; // Make the score flash green for 25 game ticks to inform the user of an increase in score
            gameScores.ai++;
        }
        else if (ball.y() + ball.height() >= wDetails.height) // Bottom wall (ball bottom >= window height)
        {
            ball.y(wDetails.height - ball.height()); // Stop the ball going off-screen
            if (abs(ball.xvel()) > 5 || abs(ball.yvel()) > 5)
	    {
		Mix_PlayChannel(SPARK_SOUND_CHANNEL, sparkSounds[rand() % (sizeof(sparkSounds) / sizeof(Mix_Chunk*))], 0);
		wpController.InitParticles(Wall::WALL_BOTTOM, windowSurface, &ball); // If the absolute value of the x or y velocities is larger than 5, initialise the particles to create a spark effect
	    }

            ball.Bounce(Wall::WALL_BOTTOM);
        }
        else if (ball.y() <= 0) // Top wall
        {
            ball.y(0); // Stop the ball going off-screen
	    if (abs(ball.xvel()) > 5 || abs(ball.yvel()) > 5)
	    {
		Mix_PlayChannel(SPARK_SOUND_CHANNEL, sparkSounds[rand() % (sizeof(sparkSounds) / sizeof(Mix_Chunk*))], 0);
		wpController.InitParticles(Wall::WALL_TOP, windowSurface, &ball); // Initialise particles as before
	    }
	    
            ball.Bounce(Wall::WALL_TOP);
        }

        for (Powerup &p : powerups)
        {
            // If the ball is touching a powerup, process the powerup
            if (ball.touching(p.objectbox()) && p.visible())
            {
                // Completely reset the powerup and hide it
                p.visible(false);
		p.text_x(p.x());
		p.text_y(p.y());
                p.x(0);
                p.y(0);
                p.ticks(0);
		p.text_ticks(250);

		Mix_PlayChannel(PWRUP_SOUND_CHANNEL, powerupSound, 0);

                switch (p.effect())
                {
                    // Paddle effects

                    case Effect::PEFFECT_INC_BRIGHT: // Increase the brightness of the paddle
                        // increase the brightness of the paddle if the brightness of the paddle + BRIGHT_CHANGE_AMOUNT < 0xEEEEEE, otherwise set the brightness to 0xEEEEEE
                        // to prevent overflow to a dark colour
                        ball.paddle()->maincolour(ball.paddle()->maincolour() + BRIGHT_CHANGE_AMOUNT > 0xEEEEEE ? 0xEEEEEE : ball.paddle()->maincolour() + BRIGHT_CHANGE_AMOUNT);
                        break;

                    case Effect::PEFFECT_DEC_BRIGHT: // Decrease the brightness of the paddle
                        // If the paddle colour - BRIGHT_CHANGE_AMOUNT < 0x111111, set the paddle colour to 0x111111, otherwise set it to the paddle colour - BRIGHT_CHANGE_AMOUNT
                        // to stop underflow to a light colour
                        ball.paddle()->maincolour(ball.paddle()->maincolour() - BRIGHT_CHANGE_AMOUNT < 0x111111 ? 0x111111 : ball.paddle()->maincolour() - BRIGHT_CHANGE_AMOUNT);
                        break;

                    case Effect::PEFFECT_INC_SIZE: // Increase the size of the paddle
                        // Increase the height of the paddle if the height + 10 < MAX_PADDLE_HEIGHT, otherwise set the paddle height to MAX_PADDLE_HEIGHT
                        // in order to  prevent the paddle filling the entire screen
                        ball.paddle()->height(ball.paddle()->height() + 10 > MAX_PADDLE_HEIGHT ? MAX_PADDLE_HEIGHT : ball.paddle()->height() + 10);
                        break;

                    case Effect::PEFFECT_DEC_SIZE: // Decrease the size of the paddle
                        // Decrease the paddle height by 10 if the paddle height - 10 > MIN_PADDLE_HEIGHT, otherwise set the height to MIN_PADDLE_HEIGHT.
                        // This is to stop the height going below 0 and causing problems
                        ball.paddle()->height(ball.paddle()->height() - 10 < MIN_PADDLE_HEIGHT ? MIN_PADDLE_HEIGHT : ball.paddle()->height() - 10);
                        break;

                    case Effect::PEFFECT_INC_SCORE: // Increase the paddle score
                        (ball.paddle()->side() == LEFT ? gameScores.player : gameScores.ai) += 1 + (rand() % 2); // If the last contacted paddle is on the left of the screen increase the player score by 1 or 2, else increase the AI score by 1 or 2
                        break;

                    case Effect::PEFFECT_DEC_SCORE: // Decrease the player score
                        (ball.paddle()->side() == LEFT ? gameScores.player : gameScores.ai) -= 1 + (rand() % 2); // if the last paddle is on the left decrease the player score by 1 or 2 otherwise decrease the AI score by 1 or 2
                        break;


			
                    // Ball Effects
			
                    case Effect::BEFFECT_INC_BRIGHT: // Increase the ball brightness
                        // increase the brightness by BRIGHT_CHANGE_AMOUNT if ball.maincolour + BRIGHT_CHANGE_AMOUNT is less than 0xEEEEEE otherwise set it to 0xEEEEEE
                        // to stop the brightness value overflowing to a darker colour
                        ball.maincolour(ball.maincolour() + BRIGHT_CHANGE_AMOUNT > 0xEEEEEE ? 0xEEEEEE : ball.maincolour() + BRIGHT_CHANGE_AMOUNT);
                        break;

                    case Effect::BEFFECT_DEC_BRIGHT: // Decrease the ball brightness
                        // only decrease the brightness by BRIGHT_CHANGE_AMOUNT if ball.maincolour - BRIGHT_CHANGE_AMOUNT is larger than 0x111111
                        // in order to prevent the ball becoming invisible, but still difficult to see
                        ball.maincolour(ball.maincolour() - BRIGHT_CHANGE_AMOUNT < 0x111111 ? 0x111111 : ball.maincolour() - BRIGHT_CHANGE_AMOUNT);
                        break;

                    case Effect::BEFFECT_INC_SIZE: // Increase the size of the ball
                    {
                        // if the width of the ball + 10 > MAX_BALL_SIZE, set the size of the ball to MAX_BALL_SIZE, otherwise set it to ball width + 10
                        int bSize = ball.width() + 10 > MAX_BALL_SIZE ? MAX_BALL_SIZE : ball.width() + 10;
                        ball.width(bSize);
                        ball.height(bSize);
                        break;
                    }

                    case Effect::BEFFECT_DEC_SIZE: // Decrease the size of the ball
                    {
                        // if the width of the ball - 10 <  MIN_BALL_SIZE, set the size to MIN_BALL_SIZE, otherwise the width of the ball - 10
                        int bSize = ball.width() - 10 < MIN_BALL_SIZE ? MIN_BALL_SIZE : ball.width() - 10;
                        ball.width(bSize);
                        ball.height(bSize);
                        break;
                    }

                   case Effect::BEFFECT_CHANGE_DIR: // Change the direction of the ball by inverting the x and y velocities
                        ball.xvel(-ball.xvel());
                        ball.yvel(-ball.yvel());
                        break;

                    case Effect::BEFFECT_TELEPORT:
			ball.y(50 + (rand() % ((wDetails.height - ball.height()) - 25))); // Set the ball y position to a random spot between 25 and the window height - 25
                        break;

                    case Effect::EFFECT_NONE:
			DEBUG("Expected Powerup value, found EFFECT_NONE");
                }

		LOG(std::string("Effect applied: " + (std::string)p.getEffectDetails(p.effect())).c_str());
            }
        }

        if ((gameTicks % 50) == 0 && !(rand() % 50)) // Every 50 ticks there is a 1 in 50 chance of a powerup appearing
        {
            // Get a random index from 0 to POWERUPS_NUMBER
            int index = rand() % POWERUPS_NUMBER;

            if (!powerups[index].visible())
            {		
		powerups[index].setRandomEffect();
                powerups[index].type((PowerupType)(rand() % (int)PowerupType::PWRUP_END)); // Choose a random powerup type from 0 (PWRUP_RED) to 2 (PWRUP_BLUE)
                powerups[index].x((rand() % (wDetails.width - 200)) + 100); // Min x: 100 Max x: window width - 100
                powerups[index].y(rand() % (wDetails.height - powerups[index].height())); // Get a random y position from 0 to the window height - the powerup height
                powerups[index].ticks(500 + (rand() % 300)); // Set the ticks the powerup is available for (at least 500 ticks)
                powerups[index].visible(true);

		char buf[3][8];
		LOG(std::string("Powerup shown ('" + Powerup::getEffectDetails(powerups[index].effect()) + "', X:"
				+ itoa(powerups[index].x(), buf[0], 10) + ", Y:" +
				itoa(powerups[index].y(), buf[1], 10) + ", Index:" +
				itoa(index, buf[2], 10) + ")").c_str());
            }
        }

        int y;
        SDL_GetMouseState(NULL, &y); // We don't require the X position of the mouse, so just pass NULL
	player1Paddle.y(y - (player1Paddle.height() / 2)); // Set the player's paddle y to the cursor y - 1/2 of paddle height

        // Prevent the paddle from going off-screen
        if (player1Paddle.y() < 0) player1Paddle.y(0);
        if (player1Paddle.y() + player1Paddle.height() > wDetails.height) player1Paddle.y(wDetails.height - player1Paddle.height());

	// Update the paddle AI
        paddleAI.Update(&ball, &aiPaddle, wDetails.width, wDetails.height);

	if (gameScores.player >= WIN_AMOUNT || gameScores.ai >= WIN_AMOUNT)
	    this->win_screen();

	// Increment the game ticks. Used in many other parts of this class
        gameTicks++;
	
        this->OnRender();
        this->Idle();
    }
}

void Controller::OnRender()
{
    // Fill the screen with black
    SDL_FillRect(windowSurface, NULL, 0x00);

    ball.Draw(windowSurface);

    // If bounceRect.x != 0 or bounceRect.y != 0.
    // These values are set when an impact occurs between the ball and a paddle
    if (bounceRect.x || bounceRect.y)
    {
        SDL_SetSurfaceAlphaMod(bounceSurface, bounceAlpha); // Modify the alpha channel of the bounce effect
        SDL_BlitScaled(bounceSurface, NULL, windowSurface, &bounceRect); // Draw the bounce effect to the screen
        bounceAlpha -= 5; // Makes the effect fade out over 51 (255 / 5) game ticks

	// Reset the bounce effect by setting the x and y to 0; and the alpha level to 255 (opaque)
        if (bounceAlpha <= 5)
        {
            bounceRect.x = 0;
            bounceRect.y = 0;
            bounceAlpha = 255;
        }
    }

    for (Powerup& p : powerups)
    {
	if (p.visible())
	{
	    p.Draw(windowSurface);
	    p.ticks(p.ticks() - 1);

	    if (!p.ticks()) p.visible(false);
	    
	} else if (p.text_visible())
	{
	    textRenderer.print(Powerup::getEffectDetails(p.effect()), windowSurface, 0xFFFFFF, p.text_x(), p.text_y(), PWRUP_TEXT_SIZE, PWRUP_TEXT_SIZE);
	    p.text_ticks(p.text_ticks() - 1);
	}
    }
    
    // Draw the paddles to screen
    player1Paddle.Draw(windowSurface);
    aiPaddle.Draw(windowSurface);

    // If the particles are on, move and draw them
    if (wpController.particles_on()) wpController.StepParticles(windowSurface);

    // Print the scores in green if either player has scored recently, otherwise print it in white.
    char buf[8];
    textRenderer.print(itoa(gameScores.player, buf, 10), windowSurface, (flashTicks && ball.paddle() != NULL && ball.paddle()->side() == LEFT ? 0x00FF00 : 0xFFFFFF),
		       player1Paddle.x() + player1Paddle.width(), player1Paddle.y(), SCOREBOARD_CHARSIZE, SCOREBOARD_CHARSIZE);
    textRenderer.print(itoa(gameScores.ai, buf, 10), windowSurface, (flashTicks && ball.paddle() != NULL && ball.paddle()->side() == RIGHT ? 0x00FF00 : 0xFFFFFF),
		       // Use a lambda here to find the number of digits in the AI's score so we can offset it from the left of the paddle appropriately
		       aiPaddle.x() - SCOREBOARD_CHARSIZE * [](int i) -> int { int count = 0; do { i /= 10; count++; } while (i); return count; }(gameScores.ai), aiPaddle.y(), SCOREBOARD_CHARSIZE, SCOREBOARD_CHARSIZE);
    flashTicks -= (flashTicks ? 1 : 0); // If flashTicks > 0 decrement it

    if (helpVisible)
    {
	textRenderer.print("Help", windowSurface, 0xEEEEEE, 100, 20, 14, 14);

	// Iterate through each line of the help message and print it to the screen
	for (unsigned int i = 0; i < sizeof(helpText) / sizeof(char*); i++)
	    textRenderer.print(helpText[i], windowSurface, 0xEEEEEE, 100, 50 + (i * 8));
    }

    SDL_UpdateWindowSurface(mainWindow);
}

// Basic event handling
void Controller::OnEvent(SDL_Event* event)
{
    switch (event->type)
    {
        case SDL_KEYDOWN: // Key pressed
        {
            switch (event->key.keysym.sym)
            {
                case SDLK_ESCAPE: this->init_menu(); break; // If the user presses the escape key, initialise the menu

		case SDLK_F1: helpVisible = !helpVisible; break;

		// Toggle mouse capture when space pressed. This is so we can move the window around to see the console easier in debugging mode
		#ifdef DBG
		case SDLK_SPACE: mouseCaptured = !mouseCaptured; SDL_SetRelativeMouseMode((SDL_bool)mouseCaptured); break;
                #endif
            }

	    break;
	}

	case SDL_QUIT: quit = true; break;
    }
}

void Controller::Cleanup()
{
    SDL_DestroyWindow(mainWindow);
    SDL_Quit(); // End SDL
}

void Controller::Idle()
{
    // Idle to prevent the CPU exploding

    while (!(SDL_GetTicks() - lastSDLticks))
    {
	#if defined _WIN32
    	    Sleep(1); // Use the native Win32 Sleep function (milliseconds)
	#elif defined __linux || defined __unix || defined __APPLE__
    	    usleep(1000); // Use the native *nix usleep function (microseconds)
    	#endif
    }
    
    lastSDLticks = SDL_GetTicks();
}

void Controller::init_menu()
{
    LOG("Init main menu");
    
    bool startGame = quit;
    bool resetGame = false;
    unsigned long menuticks = 0;

    // Positioning for the enter key
    SDL_Rect* blackRect = new SDL_Rect { 0, 0, 350, wDetails.height };
    SDL_Rect* imgRect   = new SDL_Rect { 50, 50, 250, 80 };

    Ball menuBall;

    mainMenu.ClearItems();
    mainMenu.x(25);
    mainMenu.y(wDetails.height - ((MAIN_MENU_CHARSIZE + 8) * (gameStarted ? 3 : 2)) - 25);
    mainMenu.MenuItemChange(MENU_ITEM_CHANGE, (void*)menuClick);

    mainMenuItems[0].text = gameStarted ? "Resume" : "Start";
    mainMenuItems[0].charWidth = MAIN_MENU_CHARSIZE;
    mainMenuItems[0].charHeight = MAIN_MENU_CHARSIZE;
    mainMenuItems[0].yOffset = 8;
    mainMenuItems[0].cursor = SDL_CreateSystemCursor(SDL_SYSTEM_CURSOR_HAND);

    mainMenuItems[0].userdata = &startGame;
    mainMenuItems[0].action = [](bool* data) -> void { *data = true; };

    mainMenuItems[1] = mainMenuItems[0];
    mainMenuItems[1].text = "Reset";
    mainMenuItems[1].userdata = &resetGame;

    mainMenuItems[2] = mainMenuItems[0];
    mainMenuItems[2].text = "Quit";
    mainMenuItems[2].userdata = &quit;

    mainMenu.AddItem(&mainMenuItems[0]);
    if (gameStarted) mainMenu.AddItem(&mainMenuItems[1]);
    mainMenu.AddItem(&mainMenuItems[2]);

    mainMenu.SelectedIndex(0);

    auto randNeg = []() -> int { int i = 0; while (!i) i = (rand() % 2) - (rand() % 2); return i; };
    menuBall.xvel(2 * randNeg());
    menuBall.yvel(3 * randNeg());
    menuBall.center(wDetails.width, wDetails.height);
    menuBall.maincolour(0xAAAAAA);
    menuBall.width(45);
    menuBall.height(45);

    // Release mouse capture
    SDL_SetRelativeMouseMode(SDL_FALSE);

    while (!startGame)
    {
	// Fill the screen with white
	SDL_FillRect(windowSurface, NULL, 0xFFFFFF);

	// Fill a dark gray box and the Pang logo overtop
	SDL_FillRect(windowSurface, blackRect, 0x111111);
	SDL_BlitSurface(mmLogoSurface, NULL, windowSurface, imgRect);

	menuBall.Draw(windowSurface);

	mainMenu.Draw(windowSurface);

	SDL_UpdateWindowSurface(mainWindow);

	SDL_Event event;
	while (SDL_PollEvent(&event))
	    switch (event.type)
	    {
		case SDL_QUIT: quit = true; break;
		case SDL_KEYDOWN:
		{
		    switch (event.key.keysym.sym)
		    {
			// If the user presses the escape key, prompt the user to quit the program
			case SDLK_ESCAPE: this->quit_prompt(&startGame); break;
		    }

		    break;
		}
	    }

	mainMenu.HandleInput(&event);
	if (quit) break;

	if (resetGame)
	{
	    this->reset_objects();
	    break;
	}

	menuBall.x(menuBall.x() + menuBall.xvel());
	menuBall.y(menuBall.y() + menuBall.yvel());

	if (menuBall.x() < 350)
	{
	    menuBall.x(350);
	    menuBall.Bounce(Wall::WALL_LEFT);
	} else if (menuBall.x() + menuBall.width() > wDetails.width)
	{
	    menuBall.x(wDetails.width - menuBall.width());
	    menuBall.Bounce(Wall::WALL_RIGHT);
	} else if (menuBall.y() < 0)
	{
	    menuBall.y(0);
	    menuBall.Bounce(Wall::WALL_TOP);
	} else if (menuBall.y() + menuBall.height() > wDetails.height)
	{
	    menuBall.y(wDetails.height - menuBall.height());
	    menuBall.Bounce(Wall::WALL_BOTTOM);
	}
	
	this->Idle();
	menuticks++;
    }

    // Recapture the mouse
    SDL_SetRelativeMouseMode(SDL_TRUE);

    delete blackRect;
    delete imgRect;
}

void Controller::quit_prompt(bool *exitCondition)
{
    SDL_Event event;
    bool keypressed = false;

    // Fill the screen with white and prompt the user for input
    SDL_FillRect(windowSurface, NULL, 0xFFFFFF);
    textRenderer.print("Really Quit? Y/N", windowSurface, 0xCC0000, 10, 10, 21, 21);
    SDL_UpdateWindowSurface(mainWindow);

    // Start a sub loop to handle the exit prompt
    while (!keypressed)
    {
	while (SDL_PollEvent(&event) && !keypressed)
	    switch(event.type)
	    {
		case SDL_QUIT: keypressed = true; *exitCondition = true; quit = true; break;

		case SDL_KEYDOWN:

		    switch (event.key.keysym.sym)
		    {
			// Intentional fall-throughs to provide alternate input to give the user options
			// If the user presses 'Y' or 'Enter', quit the program
			case SDLK_y: 
			case SDLK_RETURN: keypressed = true; *exitCondition = true; quit = true; break;

			// If the user presses 'N' or 'Esc' do not quit the program and return to the main menu
			case SDLK_n: 
			case SDLK_ESCAPE: keypressed = true; break;
		    }

		    break;
	    }

	this->Idle();
    }
}

void Controller::win_screen()
{
    SDL_Event event;
    bool quitScreen = false;
    bool ballSelected = false;
    int mX, mY;
    
    mouseCaptured = false;
    SDL_SetRelativeMouseMode(SDL_FALSE);

    auto randNeg = []() -> int { int i = 0; while (!i) i = (rand() % 2) - (rand() % 2); return i; }; // Returns either 1 or -1

    ball.width(DEFAULT_BALL_SIZE);
    ball.height(DEFAULT_BALL_SIZE);
    ball.xvel(2 * randNeg());
    ball.yvel(1 * randNeg());
    ball.center(wDetails.width, wDetails.height);
    ball.maincolour(0xAAAAAA);

    endMenu.ClearItems();
    endMenu.x(50);
    endMenu.y(wDetails.height - 90);
    endMenu.MenuItemChange(MENU_ITEM_CHANGE, (void*)menuClick);

    endMenuItems[0].text = "Main Menu";
    endMenuItems[0].charWidth = END_MENU_CHARSIZE;
    endMenuItems[0].charHeight = END_MENU_CHARSIZE;
    endMenuItems[0].yOffset = 16;
    endMenuItems[0].cursor = SDL_CreateSystemCursor(SDL_SYSTEM_CURSOR_HAND);

    endMenuItems[0].userdata = &quitScreen;
    endMenuItems[0].action = [](bool* data) -> void { *data = true; };
    
    endMenuItems[1] = endMenuItems[0];
    endMenuItems[1].text = "Quit Game";
    endMenuItems[1].userdata = &quit;

    endMenu.AddItem(&endMenuItems[0]);
    endMenu.AddItem(&endMenuItems[1]);

    // Used for drawing the rectangles on-screen
    SDL_Rect borders[2] =
    {
	{ 0, 0, wDetails.width, 180 },
	{ 0, wDetails.height - 120, wDetails.width, 120 }
    };

    while (!quitScreen)
    {
	SDL_FillRect(windowSurface, NULL, 0xFFFFFF);

	ball.Draw(windowSurface);
	SDL_FillRect(windowSurface, &borders[0], 0x111111);
	SDL_FillRect(windowSurface, &borders[1], 0x111111);
	textRenderer.print((std::string)(gameScores.player > gameScores.ai ? "PLAYER" : "  AI") + "\n WINS", windowSurface, 0xFFFFFF, (wDetails.width / 2) - ((6 * 70) / 2), 10, 70, 70);

	endMenu.Draw(windowSurface);

	SDL_UpdateWindowSurface(mainWindow);

	SDL_GetMouseState(&mX, &mY);

	// If the ball is not selected by the mouse, move as normal, otherwise go to the coordinates of the mouse. EASTER EGGS
	if (!ballSelected)
	{
	    ball.x(ball.x() + ball.xvel());
	    ball.y(ball.y() + ball.yvel());
	} else {
	    ball.x(mX - (ball.width() / 2));
	    ball.y(mY - (ball.height() / 2));
	}

	// Simple collision detection
	if (ball.x() <= 0)
	{
	    ball.Bounce(Wall::WALL_LEFT);
	    ball.x(0);
	} else if (ball.x() + ball.width() >= wDetails.width)
	{
	    ball.Bounce(Wall::WALL_RIGHT);
	    ball.x(wDetails.width - ball.width());
	} else if (ball.y() <= 180)
	{
	    ball.Bounce(Wall::WALL_TOP);
	    ball.y(180);
	} else if (ball.y() + ball.height() >= wDetails.height - 120)
	{
	    ball.Bounce(Wall::WALL_BOTTOM);
	    ball.y(wDetails.height - ball.height() - 120);
	}

	endMenu.HandleInput(&event);
	
	while (SDL_PollEvent(&event))
	    switch (event.type)
	    {
		case SDL_QUIT: quitScreen = true; quit = true; break;
		case SDL_KEYDOWN:
		{
		    switch (event.key.keysym.sym)
		    {
			case SDLK_ESCAPE: this->quit_prompt(&quitScreen); break;
		    }
		}

		case SDL_MOUSEBUTTONUP: ballSelected = false; break;
		case SDL_MOUSEBUTTONDOWN:
		{
		    SDL_Rect mouseRect = (SDL_Rect) { mX, mY, 1, 1 };

		    ballSelected = ball.touching(&mouseRect);
		}
	    }

	if (quit) break;

	this->Idle();
    }

    if (!quit) this->reset_objects();
}

void Controller::reset_objects()
{
    gameStarted = false;
    helpVisible = false;
    flashTicks = 0;
    lastHitTick = 0;
    gameTicks = 0;
    bounceAlpha = 0;

    ball = Ball();
    player1Paddle = Paddle();
    aiPaddle = Paddle();
    gameScores = { };

    SDL_SetCursor(DEFAULT_CURSOR);

    this->Init();
}
