#ifndef MULTICHOICE_H
#define MULTICHOICE_H

#include <algorithm>
#include <string>
#include <typeinfo>
#include <vector>

#include <cstdlib>

#include <SDL.h>

#include "GameObject.hpp"
#include "Text.hpp"
#include "log.hpp"

#define DEFAULT_CURSOR SDL_CreateSystemCursor(SDL_SYSTEM_CURSOR_ARROW)
#define MENU_CHANGE_TOLERANCE 200

template <typename UDType>
struct MultiChoiceItem
{
    std::string text;
    int charWidth = 7;
    int charHeight = 7;
    int xOffset = 0;
    int yOffset = 5;
    bool drawBg = false;
    Uint32 forecolour = 0x888888;
    Uint32 backcolour = 0xFFFFFF;
    Uint32 forecolour_sel = 0x33CCFF;
    Uint32 backcolour_sel = 0xFFFFFF;
    SDL_Cursor* cursor = DEFAULT_CURSOR;
    void (*action)(UDType) = [](UDType data) -> void { }; // Set the default action to nothing
    UDType userdata = NULL;
};

template <typename T>
class MultiChoice : public GameObject
{
    public:

    	// Define a generic function pointer type
    	typedef void (*GenericFunction)(void* data);
    
    	MultiChoice()
	{
	    index = 0;
	    lastIndex = -1;
	    lastChangeTick = 0;
	    menu_switch = NULL;

	    this->x(0);
	    this->y(0);
	}

    	int width() override { return -1; }
    	int height() override { return -1; }
    	void width(int w) override { return; }
    	void height(int h) override { return; }

    	int maincolour() override { return -1; }
    	void maincolour(Uint32 colour) override { return; }

    	void Draw(SDL_Surface* surface) override
	{
	    int i = 0;
	    for (MultiChoiceItem<T>* item : items)
	    {
		SDL_Rect itemRect =
		{
		    this->x(),
		    this->y() + (i * (item->charHeight + item->yOffset)),
		    (int)item->text.length() * item->charWidth,
		    item->charHeight
		};
				    
		if (item->drawBg)
		    SDL_FillRect(surface, &itemRect, i == index ? item->backcolour_sel : item->backcolour);

		textRenderer.print(item->text, surface, i == index ? item->forecolour_sel : item->forecolour,
				   itemRect.x, itemRect.y,
				   item->charWidth, item->charHeight);

		i++;
	    }
	}

    	void HandleInput(SDL_Event* event)
	{
	    switch (event->type)
	    {
		case SDL_KEYDOWN:

		    if (SDL_GetTicks() - lastChangeTick > MENU_CHANGE_TOLERANCE)
		    {
			switch (event->key.keysym.sym)
			{
			    case SDLK_UP: index -= !index ? 0 : 1; break;
			    case SDLK_DOWN: index += index == items.size() - 1 ? 0 : 1; break;
			    case SDLK_RETURN: items[index]->action(items[index]->userdata); break;
			}

			lastChangeTick = SDL_GetTicks();
		    }

		    break;

		case SDL_MOUSEMOTION:
		{
		    int i = 0;
		    
		    for (MultiChoiceItem<T>* item : items)
		    {
			if (this->touching_item(item, i))
			{
			    index = i;
			    touchingItem = true;
			    break;
			}

			i++;
		    }

		    if (touchingItem)
			SDL_SetCursor(items[index]->cursor);
		    else if (SDL_GetCursor() != DEFAULT_CURSOR)
			SDL_SetCursor(DEFAULT_CURSOR);

		    touchingItem = false;
		    break;
		}

		case SDL_MOUSEBUTTONUP:
		{
		    int i = 0;
		    for (MultiChoiceItem<T>* item : items)
		    {
			if (this->touching_item(item, i))
			    item->action(item->userdata);

			i++;
		    }
		}
	    }

	    if (menu_switch != NULL && index != lastIndex && lastIndex != -1) menu_switch(menu_switch_data);
	    lastIndex = index;
	}

	void AddItem(MultiChoiceItem<T>* item)
	{
	    // Remove all characters which may cause problems whe  rendering text
	    std::replace(item->text.begin(), item->text.end(), '\n', '\0');
	    std::replace(item->text.begin(), item->text.end(), '\t', '\0');
		
	    items.push_back(item);
	}

    	void ClearItems()
	{
	    items.clear();
	}

    	MultiChoiceItem<T>* SelectedItem() { return items[index]; }
    	int SelectedIndex() { return index; }
    	void SelectedIndex(int i) { index = i; }

    	GenericFunction MenuItemChange() { return menu_switch; }
    	void MenuItemChange(GenericFunction func) { this->MenuItemChange(func, NULL); }
    	void MenuItemChange(GenericFunction func, void* data) { menu_switch = func; menu_switch_data = data; }

    private:

    	int get_item_width(MultiChoiceItem<T>* item)
	{
	    return item->text.length() * item->charWidth;
	}

    	bool touching_item(MultiChoiceItem<T>* item, int ind)
	{
	    int mX, mY;
	    SDL_GetMouseState(&mX, &mY);

	    // Check if the mouse is within the bounds of the item, given the offsets of the item and the MultiChoice control
	    return mX >= this->x() + item->xOffset && mX <= this->x() + this->get_item_width(item) && mY >= this->y() + (ind * (item->charHeight + item->yOffset)) && mY <= this->y() + item->yOffset + (ind * item->charHeight) + item->charHeight;
	}

    	std::vector<MultiChoiceItem<T>*> items;
    	int index, lastIndex;
    	bool touchingItem;
    	Text textRenderer;
    	GenericFunction menu_switch;
    	void* menu_switch_data;
    	long lastChangeTick;
};

#endif
