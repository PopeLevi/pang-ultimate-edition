#include "Powerup.hpp"

Powerup::Powerup()
{
    if (!(IMG_Init(IMG_INIT_PNG) & IMG_INIT_PNG))
    {
        DEBUG("Unable to init SDL2_image");
        return;
    }

    if (!(pwrupSurface = IMG_Load(FILE_PWRUP)))
        DEBUG("Unable to load surface");

    GameObject::width(PWRUP_SIZE);
    GameObject::height(PWRUP_SIZE);

    _pwrupVisible = false;
    _textVisible = false;
}

void Powerup::Draw(SDL_Surface *surface)
{
    if (!pwrupSurface || (pwrType != PowerupType::PWRUP_RED && pwrType != PowerupType::PWRUP_GREEN && pwrType != PowerupType::PWRUP_BLUE) || !_pwrupVisible)
        return;

    SDL_Rect pRect =
	(SDL_Rect) { (pwrType == PowerupType::PWRUP_RED ? PWRUP_R_X : (pwrType == PowerupType::PWRUP_GREEN ? PWRUP_G_X : PWRUP_B_X)), 0, PWRUP_SHEET_SIZE, PWRUP_SHEET_SIZE};

    SDL_BlitScaled(pwrupSurface, &pRect,
                surface, this->objectbox());
}

std::string Powerup::getEffectDetails(Effect e)
{
    try
    {
	return effectDetails[e];
    } catch (std::logic_error err) {
	DEBUG(err.what());
	return "Unknown Powerup Type";
    }
}

// Contains descriptions for the powerups
std::map<Effect, const char*> Powerup::effectDetails =
{
    { Effect::PEFFECT_INC_BRIGHT, "Increase Paddle Brightness" },
    { Effect::PEFFECT_DEC_BRIGHT, "Decrease Paddle Brightness" },
    { Effect::PEFFECT_INC_SIZE,   "Increase Paddle Size"       },
    { Effect::PEFFECT_DEC_SIZE,   "Decrease Paddle Size"       },
    { Effect::PEFFECT_INC_SCORE,  "Increase Player Score"      },
    { Effect::PEFFECT_DEC_SCORE,  "Decrease Player Score"      },

    { Effect::BEFFECT_INC_BRIGHT, "Increase Ball Brightness"   },
    { Effect::BEFFECT_DEC_BRIGHT, "Decrease Ball Brightness"   },
    { Effect::BEFFECT_INC_SIZE,   "Increase Ball Size"         },
    { Effect::BEFFECT_DEC_SIZE,   "Decrease Ball Size"         },
    { Effect::BEFFECT_CHANGE_DIR, "Change Ball Direction"      },
    { Effect::BEFFECT_TELEPORT,   "Teleport Ball"              },

    { Effect::EFFECT_NONE, "No Effect" }
};
