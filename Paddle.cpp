#include "Paddle.hpp"

Paddle::Paddle()
{
    this->width(DEFAULT_PADDLE_WIDTH);
    this->height(DEFAULT_PADDLE_HEIGHT);
    this->maincolour(0xEEEEEE);
}

void Paddle::side(ScreenSideData s)
{
    if (!s.sWidth || !s.sHeight) return;

    switch (s.side)
    {
        case LEFT: this->x(PADDLE_WALL_OFFSET); break;
        case RIGHT: this->x((s.sWidth - PADDLE_WALL_OFFSET) - this->width()); break;
    }

    this->y((s.sHeight / 2) - (this->height() / 2));
    paddleSide = s.side;
}
