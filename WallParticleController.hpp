#ifndef WALL_PARTICLE_H
#define WALL_PARTICLE_H

#define PARTICLE_NUMBER 10

#include "Particle.hpp"
#include "Ball.hpp"

class WallParticleController
{
    public:
        WallParticleController() { particlesOn = false; particleTicks = 0; }

        bool particles_on() { return particlesOn; }
        void particles_on(bool b) { particlesOn = b; }

        int ticks() { return particleTicks; }
        void ticks(int i) { particleTicks = i; }

        void InitParticles(Wall side, SDL_Surface *windowSurface, Ball* ball)
        {
            srand(time(NULL)); // seed the random algorithm

            particlesOn = true;
            for (int i = 0; i < PARTICLE_NUMBER; i++)
            {
		int pSize = (rand() % 4) + 8; // Random particle size from 8 to 11 pixels

		// Set to a random yellow colour by removing the blue component from a random colour. This makes the particles look like sparks
                particles[i].maincolour((rand() % 256) ^ 0xFFFF00);
                particles[i].visible(true);

		// Set the width and height to a random size
                particles[i].width(pSize);
                particles[i].height(pSize);

		// Set the ball position to the ball position to make it look like the sparks are being emitted from the point of impact
                particles[i].x(ball->x());
                particles[i].y(ball->y() + (side == Wall::WALL_BOTTOM ? ball->height() : 0)); // If the wall of origin is the bottom wall, set the particle y to the ball y + height

		// Set random x and y velocities
                particles[i].xvel([]() -> int { int vel = 0; while (!vel) {vel = (rand() % 5) - (rand() % 5);} return vel; }()); // Lambda expression to get a random int from -4 to 4 not including 0
                particles[i].yvel((rand() % 9)); // Random int from 
		
                particles[i].wall(side); // Set the wall side of the particle for easier processing on the particle's side
                particles[i].Draw(windowSurface);
            }
        }

        void StepParticles(SDL_Surface *windowSurface)
        {
	    if (!particlesOn) return;
	    
            bool particlesVisible = false;
            for (int i = 0; i < PARTICLE_NUMBER; i++)
            {
                particles[i].x(particles[i].x() + particles[i].xvel()); // Increase the particle x by the x velocity
                particles[i].y(particles[i].y() + particles[i].yvel()); // Do the same with the y position
                particles[i].width(particles[i].width() - (!(particleTicks % 3) ? 1 : 0)); // Decrease the width and height of the particles every 3 ticks
                particles[i].height(particles[i].height() - (!(particleTicks % 3) ? 1 : 0));
                particlesVisible = particles[i].height(); // If the height of at least one particle is larger than 0, continue rendering particles. This is so each particle can naturally decay
                particles[i].Draw(windowSurface);
            }

            particleTicks++;
            if (!particlesVisible) this->KillParticles();
        }

        void KillParticles()
        {
            particlesOn = false;
	    // Hide all of the particles
            for (int i = 0; i < PARTICLE_NUMBER; i++)
                particles[i].visible(false);

            particleTicks = 0;
        }


    private:
        Particle particles[PARTICLE_NUMBER];

        bool particlesOn;
        int particleTicks;
};

#endif
