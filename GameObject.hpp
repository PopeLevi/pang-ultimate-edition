#ifndef GAMEOBJ_H
#define GAMEOBJ_H

#include <SDL.h>

enum class Wall
{
    WALL_LEFT,
    WALL_TOP,
    WALL_RIGHT,
    WALL_BOTTOM,
    WALL_NONE
};

class GameObject
{
    public:

    	virtual ~GameObject() {}

    	int x() { return box.x; }
        int y() { return box.y; }
        void x(int X) { box.x = X; }
        void y(int Y) { box.y = Y; }

        virtual int width() { return box.w; }
        virtual int height() { return box.h; }
        virtual void width(int w) { box.w = w; }
        virtual void height(int h) { box.h = h; }

        SDL_Rect* objectbox() { return &box; }

        virtual int maincolour() { return mainColour; }
        virtual void maincolour(Uint32 colour) { mainColour = colour; }

    	virtual void Draw(SDL_Surface* surface) { SDL_FillRect(surface, &box, mainColour); }

    private:
        SDL_Rect box;
        Uint32 mainColour = 0xFFFFFF;

    protected:
        bool _touching(SDL_Rect *rect)
        {
	    // Return true if any side of the provided rectange is intersecting the rectangle of the game object
            return rect->x + rect->w >= this->x() && rect->y + rect->h >= this->y() && rect->x <= this->x() + this->width() && rect->y <= this->y() + this->height();
        }

};

#endif
