#ifndef PADDLE_H
#define PADDLE_H

#define DEFAULT_PADDLE_WIDTH 20
#define DEFAULT_PADDLE_HEIGHT 175
#define MIN_PADDLE_HEIGHT 75
#define MAX_PADDLE_HEIGHT 375
#define PADDLE_WALL_OFFSET 30

#include "GameObject.hpp"

enum PaddleSide
{
    PADDLE_NONE,
    PADDLE_TOP,
    PADDLE_BOTTOM,
    PADDLE_LEFT,
    PADDLE_RIGHT
};

enum ScreenSide
{
    LEFT,
    RIGHT
};

struct ScreenSideData
{
    ScreenSide side = LEFT;
    int sWidth = 0;
    int sHeight = 0;
};

class Paddle : public GameObject
{
    public:
    	Paddle();

        ScreenSide side() { return paddleSide; }
        void side(ScreenSideData s);

        bool touching(SDL_Rect *rect) { return GameObject::_touching(rect); }

    private:
        ScreenSide paddleSide;
};

#endif
