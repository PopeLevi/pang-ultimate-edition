#ifndef PADDLE_AI_H
#define PADDLE_AI_H

#define INITIAL_DET_RANGE 250

#include <algorithm>

#include <cmath>

#include "Ball.hpp"
#include "Paddle.hpp"

class PaddleAI
{
    public:
        PaddleAI() { detectionRange = INITIAL_DET_RANGE; }

        void Update(Ball *ball, Paddle* aiPaddle, int screenWidth, int screenHeight)
        {
            int ballMax = std::max(abs(ball->xvel()), abs(ball->yvel())); // The max value of xvel and yvel
            speed = ballMax < 8 ? ceil((ballMax / 2)) + 2 : ballMax - 2; // Set the max speed of the paddle to half of the ball's larger velocity + 2

            if (ballMax - 5 > 0)
                detectionRange = INITIAL_DET_RANGE + ((ballMax - 5) * 5); // If the speed of the ball is fast enough, increase the detection range

            // Check for ball position when it is within detection range and move based on this
            if (ball->x() + ball->width() >= (aiPaddle->x() + aiPaddle->width()) - detectionRange)
            {
		// If the y position of the ball is greater than the y of the middle of the paddle, increase the paddle y
                if (ball->y() > aiPaddle->y() + (aiPaddle->height() / 2))
                {
                    aiPaddle->y(aiPaddle->y() + speed);
                } else if (ball->y() < aiPaddle->y() + (aiPaddle->height() / 2)) // Do the same, but decrease the paddle y
                {
                    aiPaddle->y(aiPaddle->y() - speed);
                }
            } else
	    {
		// If the center of the paddle is smaller than half the window's height, step towards it. This gives the AI a better chance
		// of hitting the ball
		if (aiPaddle->y() + (aiPaddle->height() / 2) < (screenHeight / 2) - 10)
		    aiPaddle->y(aiPaddle->y() + speed);
		// Do the same here, but if the center of the paddle is larger than half the window's height
		else if (aiPaddle->y() + (aiPaddle->height() / 2) > (screenHeight / 2) + 10)
		    aiPaddle->y(aiPaddle->y() - speed);
	    }

	    // Prevent the paddle leaving the screen
            if (aiPaddle->y() < 0) aiPaddle->y(0);
            else if (aiPaddle->y() + aiPaddle->height() > screenHeight) aiPaddle->y(screenHeight - aiPaddle->height()); // Prevent the paddle going outside the screen bounds
        }

    private:
        int speed;
        int detectionRange;
};

#endif
