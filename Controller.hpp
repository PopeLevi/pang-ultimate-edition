#ifndef CONTROLLER_H
#define CONTROLLER_H

#if defined _WIN32
    #include <windows.h> // Sleep(milliseconds)
#elif defined __linux || defined __unix || defined __APPLE__
    #include <unistd.h> // usleep(microseconds)
#endif

#include <string>

#include <SDL.h>
#include <SDL_mixer.h>

#include "Ball.hpp"
#include "Paddle.hpp"
#include "PaddleAI.hpp"
#include "WallParticleController.hpp"
#include "Powerup.hpp"
#include "Text.hpp"
#include "MultiChoice.hpp"
#include "filenames.hpp"
#include "log.hpp"

#define SCOREBOARD_CHARSIZE 25
#define POWERUPS_NUMBER 5

#define SPARK_SOUND_CHANNEL 1
#define PADDLE_SOUND_CHANNEL 2
#define PWRUP_SOUND_CHANNEL 3
#define MENU_SOUND_CHANNEL 4

#define SPARK_SOUND_VOL 4
#define PADDLE_SOUND_VOL 32
#define PWRUP_SOUND_VOL 16
#define MENU_SOUND_VOL 16

#define WIN_AMOUNT 20

#define MAIN_MENU_CHARSIZE 49
#define END_MENU_CHARSIZE 21
#define MENU_ITEM_CHANGE [](void* data) -> void { Mix_PlayChannel(MENU_SOUND_CHANNEL, (Mix_Chunk*)data, 0); }

struct WindowDetails
{
    const char* title = "SDL_Window";
    int x = SDL_WINDOWPOS_UNDEFINED;
    int y = SDL_WINDOWPOS_UNDEFINED;
    int width = 640;
    int height = 480;
    Uint32 style = SDL_WINDOW_SHOWN;
};

struct Scores
{
    int player = 0;
    int ai = 0;
};

class Controller
{
    public:

    	Controller();
    	Controller(WindowDetails *details);
        ~Controller();

        virtual void Init();
        virtual void OnLoop();
        virtual void OnEvent(SDL_Event *event);
        virtual void OnRender();
        virtual void Cleanup();
        void Idle();

    private:
        void init(WindowDetails *details);
    	void init_menu();
    	void quit_prompt(bool *exitCondition);
    	void win_screen();
    	void reset_objects();

    	bool windowExists;
        bool quit;
    	bool gameStarted;
    	bool mouseCaptured;
    	bool helpVisible;
        int flashTicks;
        int refreshRate;
        unsigned long lastHitTick;
        unsigned long gameTicks;
        uint8_t bounceAlpha;
        WindowDetails wDetails;
    unsigned long lastSDLticks;

    	const char* sparkSoundFiles[5] = 
	{
	    FILE_SPARK1,
	    FILE_SPARK2,
	    FILE_SPARK3,
	    FILE_SPARK4,
	    FILE_SPARK5
	};

    	const char* paddleSoundFiles[3] =
	{
	    FILE_PADDLEIMPACT1,
	    FILE_PADDLEIMPACT2,
	    FILE_PADDLEIMPACT3
	};

    	const char* helpText[10] =
	{
	    "You are the player on the left side",
	    "of the screen.",
	    "Control your paddle using the mouse.",
	    "During the course of the game",
	    "Powerups will appear, which are ",
	    "collected by the ball and applied",
	    "to the last player in contact with",
	    "the ball, or to the ball itself.",
	    "",
	    "Press ESC to pause"
	};

    	Mix_Chunk *sparkSounds[5];
    	Mix_Chunk *paddleSounds[3];
    	Mix_Chunk *powerupSound;
	Mix_Chunk *menuClick;

        SDL_Window *mainWindow;
        SDL_Surface *windowSurface;
        SDL_Surface *iconSurface;
    	SDL_Surface *mmLogoSurface;
    	SDL_Surface *mmEnterPrompt1;
    	SDL_Surface *mmEnterPrompt2;
        SDL_Surface *bounceSurface;
        SDL_Rect bounceRect;

        Ball ball;
        Text textRenderer;
        Powerup powerups[POWERUPS_NUMBER];
        Paddle player1Paddle;
        Paddle aiPaddle;
        PaddleAI paddleAI;
        Scores gameScores;
        WallParticleController wpController;

    	MultiChoice<bool*> mainMenu;
    	MultiChoiceItem<bool*> mainMenuItems[3];
    
    	MultiChoice<bool*> endMenu;
    	MultiChoiceItem<bool*> endMenuItems[2];
};

#endif
