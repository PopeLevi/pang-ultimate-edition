//Images
#define FILE_LOGO		"img/logo.png" // Main menu logo
#define FILE_ENTER1		"img/enter_glyph1.png" // Enter key glyphs (pressed and unpressed states)
#define FILE_ENTER2		"img/enter_glyph2.png"
#define FILE_CHARMAP 		"img/charmap.png" // Map for characters (letter/numbers)
#define FILE_WINICON 		"img/icon.png" // Main window icon
#define FILE_PWRUP 		"img/powerups.png" // Powerup textures
#define FILE_PBOUNCE 		"img/paddle_bounce.png" // Ball bounce texture
#define FILE_PWRUP_GLYPHMAP 	"img/powerup_effects.png" // Powerup effect icons

//Sounds
#define FILE_SPARK1 		"sound/spark1.wav" // Wall spark sound effects
#define FILE_SPARK2		"sound/spark2.wav"
#define FILE_SPARK3 		"sound/spark3.wav"
#define FILE_SPARK4		"sound/spark4.wav"
#define FILE_SPARK5		"sound/spark5.wav"
#define FILE_PADDLEIMPACT1	"sound/paddle_impact1.wav" // Paddle impact sound effects
#define FILE_PADDLEIMPACT2	"sound/paddle_impact2.wav"
#define FILE_PADDLEIMPACT3	"sound/paddle_impact3.wav"
#define FILE_PWRUP_SOUND	"sound/powerup.wav" // Powerup pickup sound
#define FILE_MENU_CLICK		"sound/menu_beep.wav" // Menu item selection sound
