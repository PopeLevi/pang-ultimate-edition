#ifndef POWERUP_H
#define POWERUP_H

#define PWRUP_R_X 0
#define PWRUP_G_X 16
#define PWRUP_B_X 32

#define PWRUP_SHEET_SIZE 16
#define PWRUP_SIZE 96

#define PWRUP_TEXT_SIZE 7

#define BRIGHT_CHANGE_AMOUNT 0x222222

#include <string>
#include <map>

#include <ctime>
#include <cstdlib>
#include <cassert>

#include <SDL.h>
#include <SDL_image.h>

#include "GameObject.hpp"
#include "filenames.hpp"
#include "log.hpp"

enum class PowerupType : int
{
    PWRUP_RED,
    PWRUP_GREEN,
    PWRUP_BLUE,
	
    PWRUP_END
};

enum class Effect : int
{
    // Paddle effects
    PEFFECT_INC_BRIGHT = 0x00,
    PEFFECT_DEC_BRIGHT = 0x01,
    PEFFECT_INC_SIZE = 0x02,
    PEFFECT_DEC_SIZE = 0x03,
    PEFFECT_INC_SCORE = 0x04,
    PEFFECT_DEC_SCORE = 0x05,

    // Ball effects
    BEFFECT_INC_BRIGHT = 0x06,
    BEFFECT_DEC_BRIGHT = 0x07,
    BEFFECT_INC_SIZE = 0x08,
    BEFFECT_DEC_SIZE = 0x09,
    BEFFECT_CHANGE_DIR = 0x0a,
    BEFFECT_TELEPORT = 0x0b,

    EFFECT_NONE = 0x0c
};

class Powerup : public GameObject
{
    public:
        Powerup();
        PowerupType type() { return pwrType; }
        void type(PowerupType pc) { pwrType = pc; }

        unsigned long ticks() { return _ticks; }
        void ticks(unsigned long t) { _ticks = t; }

        int width() override { return GameObject::width(); }
        void width(int w) override { return; }
        int height() override { return GameObject::height(); }
        void height(int h) override { return; }

        bool visible() { return _pwrupVisible; }
        void visible(bool b) { _pwrupVisible = b; }

	int text_ticks() { return _textTicks; }
    	void text_ticks(int i) { _textTicks = i; _textVisible = (bool)i; }

    	bool text_visible() { return _textVisible; }

    	int text_x() { return textx; }
    	void text_x(int i) { textx = i; }

    	int text_y() { return texty; }
    	void text_y(int i) { texty = i; }

        void Draw(SDL_Surface *surface) override;

        Effect effect() { return _effect; }
    	void effect(Effect e) { _effect = e; }
        void setRandomEffect()
	{
	    srand(time(NULL));
	    _effect = Effect::EFFECT_NONE;
	    while (_effect == Effect::EFFECT_NONE) // Randomize _effect until != EFFECT_NONE 
		_effect = (Effect)(rand() % (int)Effect::EFFECT_NONE);
	}

    	static std::string getEffectDetails(Effect e);

    private:

    	PowerupType pwrType;
        SDL_Surface *pwrupSurface;
        Effect _effect;
        unsigned long _ticks;
    	int _textTicks;
    	int textx;
    	int texty;
        bool _pwrupVisible;
	bool _textVisible;

    	static std::map<Effect, const char*> effectDetails;
};

#endif
