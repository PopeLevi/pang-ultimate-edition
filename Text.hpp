#ifndef TEXT_H
#define TEXT_H

#define CHAR_WIDTH 7
#define CHAR_HEIGHT 7
#define TAB_LENGTH 4

#include <map>

#include <SDL.h>
#include <SDL_image.h>

#include "filenames.hpp"

class Text
{
    public:
        Text()
        {
            // Initialise PNG mode so we can load PNG images into memory
            IMG_Init(IMG_INIT_PNG);
            charmap = IMG_Load(FILE_CHARMAP); // Load the character map image into charmap

            // Load all 52 characters of the alphabet into charPoints. This is for easy access to the characters on the sheet
            for (int i = 0; i < 26; i++)
            {
                charPoints[(char)i + 'A'].y = 0; // 0 pixels from top of the image
                charPoints[(char)i + 'A'].x = i * 6;
                charPoints[(char)i + 'a'].y = 7; // 7 pixels from top of the image
                charPoints[(char)i + 'a'].x = i * 6;
            }

            // Load numbers and some characters into charPoints also
            for (int i = 0; i <= 14; i++) // + , - . / 0 1 2 3 4 5 6 7 8 9
            {
                charPoints[(char)i + '+'].y = 15;
                charPoints[(char)i + '+'].x = i * 6;
            }

	    charPoints['?'].y = 15;
	    charPoints['?'].x = 90;
        }

        void print(std::string str, SDL_Surface* dest, Uint32 colour = 0xFFFFFF, int x = 0, int y = 0, int w = 7, int h = 7)
        {
            SDL_SetSurfaceColorMod(charmap, (colour >> 16) & 0xFF, (colour >> 8) & 0xFF, colour & 0xFF); // Set the colour of the text by getting the RGB values from the provided colour
            // RED: (colour >> 16) & 0xFF, GREEN: (colour >> 8) & 0xFF, BLUE: colour & 0xFF

            int i = 0;
            for (char c : str)
            {
                // If charPoints contains the requested character, blit it to the destination surface, otherwise skip over the character and produce a blank space
		// In order to avoid undefined output
		if (charPoints.find(c) != charPoints.end())
		{
		    SDL_Rect charRect = (SDL_Rect) { charPoints[c].x, charPoints[c].y, CHAR_WIDTH, CHAR_HEIGHT };
		    SDL_Rect destRect = (SDL_Rect) { x + i, y, w, h };

		    SDL_BlitScaled(charmap, &charRect, dest, &destRect);
		    i += w;
		} else if (c == '\n')
		{
		    y += h + (h / CHAR_HEIGHT);
		    i = 0;
		} else if (c == ' ')
		    i += w;
		else if (c == '\t')
		    i += w * TAB_LENGTH;
            }
        }

    private:

        struct point
        {
            int x;
            int y;
        };

        std::map<char,point> charPoints;
        SDL_Surface *charmap;
};

#endif
