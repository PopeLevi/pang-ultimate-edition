#ifndef PARTICLE_H
#define PARTICLE_H

#include <cstdlib>
#include <ctime>
#include <cmath>

#include "GameObject.hpp"

class Particle : public GameObject
{
    public:
        Particle()
        {
            srand(time(NULL));
            _visible = false;

            xVelocity = 0;
            yVelocity = 0;

            while (!xVelocity)
                xVelocity = (rand() % 4) - (rand() % 4);

            while (!yVelocity)
                yVelocity = rand() % 8;
        }

        Wall wall() { return wallSide; }
        void wall(Wall w) { wallSide = w; }

        int xvel() { return xVelocity; }
        int yvel() { return yVelocity; }
        void xvel(int vel) { xVelocity = vel; }
        void yvel(int vel) { yVelocity = vel; }

        bool visible() { return _visible; }
        void visible(bool v) { _visible = v; }

        void Draw(SDL_Surface *surface) override
        {
            if (!_visible) return;

            switch (wallSide)
            {
		case Wall::WALL_NONE: break;
                case Wall::WALL_TOP: yVelocity = abs(yVelocity); break;
		case Wall::WALL_LEFT: xVelocity = abs(xVelocity); break;
		case Wall::WALL_BOTTOM: yVelocity = -abs(yVelocity); break;
		case Wall::WALL_RIGHT: xVelocity = -abs(xVelocity); break;
                default: return;
            }

            GameObject::Draw(surface);
       }

    private:
        Wall wallSide = Wall::WALL_NONE;
        int xVelocity, yVelocity;
        int particleSize;
        bool _visible;
};

#endif
