#ifndef LOG_H
#define LOG_H

// Account for structural differences between operating systems otherwise directory-related problems would occur
#if defined _WIN32
    #define PATH_SEPERATOR "\\"
#elif defined __linux || defined __unix || defined __APPLE__
    #define PATH_SEPERATOR "/"
#else // Any other operating systems are likely to have a Unix-like structure
    #define PATH_SEPERATOR "/"
#endif

#include <string>

#include <cstdio>
#include <cstdlib>
#include <ctime>

#ifdef DBG
    #define DEBUG(msg) printf("%s: %s, ln%i:\t%s\n", logger::get_filename(__FILE__).c_str(), __FUNCTION__, __LINE__, msg) // Prints the current file, function and line. Generally used for printing errors
    #define LOG(msg) printf("%s :\t%s\n", logger::get_datetime_string().c_str(), msg) // Prints logging information (date, time and a message) to the screen
#else
    #define DEBUG(msg) []() -> void { }() // Use empty lambdas to stop annoying 'Statement has no effect' warnings during compilation
    #define LOG(msg) []() -> void { }()
#endif

class logger
{
    public:
    
    	static std::string get_filename(std::string path)
	{
	    // Finds the last index of PATH_SEPERATOR and returns the path from that point onward if the index is valid. Otherwise returns the entire path.
	    // This is to retrieve just the filename from a given path
	    return path.find_last_of(PATH_SEPERATOR) == std::string::npos ? path : path.substr(path.find_last_of(PATH_SEPERATOR) + 1);
	}

	// Gets the current time and date and returns a string containing these values.
	// Used for logging info
    	static std::string get_datetime_string()
	{
	    time_t currentTime;
	    tm* ltime;

	    // Get the local time and store in ltime
	    time(&currentTime);
	    ltime = localtime(&currentTime);

	    // return DD-MM-YYYY hh:mm:ss
	    // By retrieving the integer values from ltime and converting to a char* and casting to a std::string
	    char buf[6][8];
	    return (std::string) itoa(ltime->tm_mday, buf[0], 10) + "-" + itoa(ltime->tm_mon + 1, buf[1], 10) + "-" +
				 itoa(ltime->tm_year + 1900, buf[2], 10) + " " + itoa(ltime->tm_hour, buf[3], 10) + ":" +
				 itoa(ltime->tm_min, buf[4], 10) + ":" + itoa(ltime->tm_sec, buf[5], 10);
	}
};

#endif
