#include "Controller.hpp"

int main(int argc, char* argv[])
{
    WindowDetails det;
    det.title = "Pang Ultimate Edition";
    det.width = 1200;
    det.height = 780;

    Controller c(&det);
    c.Init();

    return 0;
}
