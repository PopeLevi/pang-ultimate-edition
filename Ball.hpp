#ifndef BALL_H
#define BALL_H

#define MAX_BALL_SPEED 12

#define DEFAULT_BALL_SIZE 25
#define MIN_BALL_SIZE 5
#define MAX_BALL_SIZE 75

#include "GameObject.hpp"
#include "Paddle.hpp"
#include "log.hpp"

class Ball : public GameObject
{
    public:
        Ball()
        {
            this->x(0);
            this->y(0);
            this->height(DEFAULT_BALL_SIZE);
            this->width(DEFAULT_BALL_SIZE);
            paddleHits = 0;
            lastPaddle = NULL;
        }

        void Bounce(Wall w)
        {
            switch (w)
            {
                case Wall::WALL_TOP: // Intentional fall-throughs as the same code applies to adjacent walls
                case Wall::WALL_BOTTOM: yVelocity = -yVelocity; break;

                case Wall::WALL_LEFT:
                case Wall::WALL_RIGHT: xVelocity = -xVelocity; break;

		default: DEBUG("Ball.Bounce called with invalid wall side");
            }
	}

        int xvel() { return xVelocity; }
        int yvel() { return yVelocity; }
        void xvel(int vel) { xVelocity = vel; }
        void yvel(int vel) { yVelocity = vel; }

    	void center(int wWidth, int wHeight)
	{
	    this->x((wWidth / 2) - (this->width() / 2));
	    this->y((wHeight / 2) - (this->height() / 2));
	}

        int hits() { return paddleHits; }
        void add_hit() { paddleHits++; }

        Paddle *paddle() { return lastPaddle; }
        void paddle(Paddle *p) { lastPaddle = p; }

        bool touching(SDL_Rect* rect) { return GameObject::_touching(rect); }

    private:
        int xVelocity, yVelocity;
        unsigned int paddleHits;
        Paddle *lastPaddle;
};

#endif
